import socket
import random

urls = ['https://www.urjc.es/', 'https://gsyc.urjc.es/']

def generatenumber():
    return random.randint(1000000, 2200203220)

def chooseredir(myport):
    if random.choice([True, False]):  
        number = generatenumber()
        url = f"http://localhost:{myport}/{number}"
        return url, True
    else:
        return random.choice(urls), False

if __name__ == "__main__":
    myport = 1234

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(('', myport))
    s.listen(5)

    print(f"Server in {myport}...")

    while True:
        rec_socket, address = s.accept()
        print(f"Connected: {address}")

        received = rec_socket.recv(2048).decode('utf-8')

        if not received.startswith("GET"):
            rec_socket.close()
            continue

        redirto, islocal = chooseredir(myport)

        if islocal:
            response = ("HTTP/1.1 200 OK\r\n"
                        "Content-Type: text/html\r\n"
                        "\r\n"
                        "<html><body><h1>Hola!</h1></body></html>")
        else:
            response = (f"HTTP/1.1 307 Temporary Redirect\r\n"
                        f"Location: {redirto}\r\n"
                        "\r\n")

        rec_socket.send(response.encode('utf-8'))
        rec_socket.close()
